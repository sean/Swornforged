defmodule Swornforged.Dataforged do
  def cache_dataforged() do
    version = Application.get_env(:swornforged, :dataforged_version)

    manifest =
      Req.get!("https://registry.npmjs.org/dataforged/")
      |> Map.get(:body)

    dataforged =
      get_in(manifest, ["versions", version, "dist", "tarball"])
      |> Req.get!()
      |> Map.get(:body)

    dataforged_json =
      dataforged
      |> Enum.find(fn curr ->
        case curr do
          {'package/dist/starforged/dataforged.json', _str} -> true
          _ -> false
        end
      end)
      |> then(fn {_, json_str} -> json_str end)
      |> Jason.decode!()

    Cachex.put_many!(:swornforged, dataforged_raw: dataforged, dataforged_json: dataforged_json)
  end
end
