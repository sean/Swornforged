defmodule Swornforged.KernelHelpers do
  def get_in(data, keys, default) do
    case get_in(data, keys) do
      nil -> default
      result -> result
    end
  end
end
