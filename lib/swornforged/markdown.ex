defmodule Swornforged.Markdown do
  @cache_prefix "HTML_CACHE::"

  def get_cache_html!(markdown, key, options \\ []) do
    case Cachex.get(:swornforged, @cache_prefix <> key) do
      {:ok, nil} ->
        html = Earmark.as_html!(markdown, options)
        Cachex.put!(:swornforged, @cache_prefix <> key, html)
        html
      {:ok, html} -> html
    end
  end
end
