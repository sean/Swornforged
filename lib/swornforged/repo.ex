defmodule Swornforged.Repo do
  use Ecto.Repo,
    otp_app: :swornforged,
    adapter: Ecto.Adapters.SQLite3
end
