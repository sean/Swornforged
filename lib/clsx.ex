defmodule Clsx do
  @moduledoc """
  A pure function for generating CSS classes.
  """

  @doc false
  defmacro __using__(_opts) do
    quote do
      import Clsx, only: [clsx: 1]
    end
  end

  @doc """
  Returns a string containing CSS.

  The argument can be any of the following:

    * String or atom containing a CSS class or classes.
    * Keyword list or map with string keys. The key is a CSS class and the value is
      treated as a boolean indicating whether or not the class should be included.
    * `nil` and `false` are filtered.
    * Lists, including deeply nested lists, containing any of the above types.

  This function does not dedupe classes.

  ## Examples

      iex> clsx("my-class")
      "my-class"

      iex> clsx(["one", :two])
      "one two"

      iex> clsx("yes": true, "no": false)
      "yes"

      iex> clsx(%{"string-key" => true})
      "string-key"

      iex> clsx(yes: "I'm truthy", no: nil)
      "yes"

      iex> clsx([%{"one" => true}, [two: false, three: true]])
      "one three"

      iex> clsx([{"string-key-tuple", true}, {"also-happens-to_work", nil}])
      "string-key-tuple"

      iex> clsx([[[nil, false, "hello", :world]]])
      "hello world"

      iex> clsx([{"invalid argument"}])
      ** (FunctionClauseError) no function clause matching in Clsx.clsx/1
  """
  def clsx(class) do
    [class]
    |> List.flatten()
    |> Enum.flat_map(&class_list/1)
    |> Enum.join(" ")
  end

  defguardp is_class(c) when is_binary(c) or is_atom(c)

  defp class_list(false), do: []
  defp class_list(nil), do: []
  defp class_list(s) when is_class(s), do: [s]
  defp class_list({k, false}) when is_class(k), do: []
  defp class_list({k, nil}) when is_class(k), do: []
  defp class_list({k, _}) when is_class(k), do: [k]
  defp class_list(%{} = map), do: Enum.flat_map(map, &class_list/1)
end
