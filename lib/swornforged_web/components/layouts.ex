defmodule SwornforgedWeb.Layouts do
  use SwornforgedWeb, :html

  @ci_commit_sha System.get_env("CI_COMMIT_SHA", "DEV")

  embed_templates "layouts/*"

  def ci_commit_sha(), do: @ci_commit_sha

  def ci_commit_sha_short(), do: String.slice(ci_commit_sha(), 0..9)

  def ci_commit_sha_url() do
    case ci_commit_sha() do
      "DEV" -> "/"
      sha -> "https://codeberg.org/sean/Swornforged/src/commit/" <> sha
    end
  end
end
