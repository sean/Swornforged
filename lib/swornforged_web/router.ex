defmodule SwornforgedWeb.Router do
  use SwornforgedWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {SwornforgedWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SwornforgedWeb do
    pipe_through :browser

    get "/", PageController, :home
  end

  scope "/srd/", SwornforgedWeb do
    pipe_through :browser

    get "/assets/", SrdController, :asset_index
    get "/assets/asset/:id", SrdController, :asset
    get "/assets/:subsection/", SrdController, :asset_subsection
    get "/moves/", SrdController, :move_index
    get "/moves/move/:id", SrdController, :move
    get "/moves/:subsection/", SrdController, :move_subsection
    get "/oracles/", SrdController, :oracle_index
    get "/oracles/oracle/:id", SrdController, :oracle
    get "/oracles/:subsection/", SrdController, :oracle_subsection
    get "/encounters/", SrdController, :encounter_index
    get "/encounters/encounter/:id", SrdController, :encounter
    get "/encounters/:subsection/", SrdController, :encounter_subsection
  end

  # Other scopes may use custom stacks.
  # scope "/api", SwornforgedWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:swornforged, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: SwornforgedWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end

    scope "/dev", SwornforgedWeb do
      pipe_through :browser

      get "/new-layout-test", SrdController, :new_layout_test
    end
  end
end
