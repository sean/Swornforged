defmodule SwornforgedWeb.PageHTML do
  use SwornforgedWeb, :html

  embed_templates "page_html/*"

  def srd_category_card(assigns) do
    assigns =
      assign(assigns,
        card_colors: %{
          :asset_types => "bg-sky-500 hover:bg-sky-400 dark:bg-sky-900 border-sky-500/50 dark:focus:border-sky-500/100 dark:focus:bg-sky-800 dark:hover:border-sky-500/100 dark:hover:bg-sky-800 light:shadow-sky-700/25 light:hover:shadow-sky-700/50",
          :encounters => "bg-red-500 hover:bg-red-400 dark:bg-red-900 border-red-500/50 dark:focus:border-red-500/100 dark:focus:bg-red-800 dark:hover:border-red-500/100 dark:hover:bg-red-800 light:shadow-red-700/25 light:hover:shadow-red-700/50",
          :move_categories => "bg-emerald-500 hover:bg-emerald-400 dark:bg-emerald-900 border-emerald-500/50 dark:focus:border-emerald-500/100 dark:focus:bg-emerald-800 dark:hover:border-emerald-500/100 dark:hover:bg-emerald-800 light:shadow-emerald-700/25 light:hover:shadow-emerald-700/50",
          :oracle_categories => "bg-purple-500 hover:bg-purple-400 dark:bg-purple-900 border-purple-500/50 dark:focus:border-purple-500/100 dark:focus:bg-purple-800 dark:hover:border-purple-500/100 dark:hover:bg-purple-800 light:shadow-purple-700/25 light:hover:shadow-purple-700/50"
        }
      )

    ~H"""
    <a
      href={@to}
      class={[
        "block py-8 md:py-12 lg:py-16 grid place-items-center text-white text-xl text-center md:text-2xl lg:text-3xl text-white rounded text-shadow
         shadow-xl focus:shadow-2xl hover:shadow-2xl transition-shadow border-2 border-solid border-black border-opacity-10",
        @card_colors[@category]
      ]}
    >
      <%= @title %>
    </a>
    """
  end
end
