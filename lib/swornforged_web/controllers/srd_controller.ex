defmodule SwornforgedWeb.SrdController do
  use SwornforgedWeb, :controller

  def new_layout_test(conn, _params) do
    conn = put_layout(conn, {SwornforgedWeb.Layouts, :new})
    render(conn, :move_index, page_title: "Moves", moves: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Move Categories"]))
  end

  def asset_index(conn, _params) do
    render(conn, :asset_index, page_title: "Assets", assets: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Asset Types"]))
  end

  def asset_subsection(conn, _params) do
    render(conn, :asset_index, page_title: "Assets", assets: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Asset Types"]))
  end

  def asset(conn, _params) do
    render(conn, :asset_index, page_title: "Assets", assets: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Asset Types"]))
  end

  def move_index(conn, _params) do
    render(conn, :move_index, page_title: "Moves", moves: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Move Categories"]))
  end

  def move_subsection(conn, _params) do
    render(conn, :move_index, page_title: "Moves", moves: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Move Categories"]))
  end

  def move(conn, _params) do
    render(conn, :move_index, page_title: "Moves", moves: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Move Categories"]))
  end

  def oracle_index(conn, _params) do
    render(conn, :oracle_index, page_title: "Oracles", oracles: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Oracle Categories"]))
  end

  def oracle_subsection(conn, _params) do
    render(conn, :oracle_index, page_title: "Oracles", oracles: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Oracle Categories"]))
  end

  def oracle(conn, _params) do
    render(conn, :oracle_index, page_title: "Oracles", oracles: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Oracle Categories"]))
  end

  def encounter_index(conn, _params) do
    render(conn, :encounter_index, page_title: "Encounters", encounters: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Encounters"]))
  end

  def encounter_subsection(conn, _params) do
    render(conn, :encounter_index, page_title: "Encounters", encounters: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Encounters"]))
  end

  def encounter(conn, _params) do
    render(conn, :encounter_index, page_title: "Encounters", encounters: get_in(Cachex.get!(:swornforged, :dataforged_json), ["Encounters"]))
  end
end
