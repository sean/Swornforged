defmodule SwornforgedWeb.PageController do
  use SwornforgedWeb, :controller

  def home(conn, _params) do
    render(conn, :home, page_title: "Home", dataforged: Cachex.get!(:swornforged, :dataforged_json))
  end
end
