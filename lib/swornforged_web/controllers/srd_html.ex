defmodule SwornforgedWeb.SrdHTML do
  use SwornforgedWeb, :html

  embed_templates "srd_html/*"

  def srd_type_title(assigns) do
    ~H"""
    <h2 class="text-4xl md:text-5xl lg:text-6xl dark:text-white font-bold"><%= render_slot(@inner_block) %></h2>
    """
  end

  def srd_subtype_title(assigns) do
    ~H"""
    <h3 class="text-2xl font-bold mb-2 dark:text-white"><%= render_slot(@inner_block) %></h3>
    """
  end

  def srd_type_container(assigns) do
    ~H"""
    <div class="flex flex-col gap-4">
      <%= render_slot(@inner_block) %>
    </div>
    """
  end

  def srd_item_inner_title(assigns) do
    ~H"""
    <h4 class="font-bold dark:text-white"><%= render_slot(@inner_block) %></h4>
    """
  end

  def srd_item(assigns) do
    ~H"""
    <div class="shadow-md dark:shadow-black/25 bg-white/50 dark:bg-zinc-700/60 rounded-b rounded-t">
      <h3
        class="font-bold text-white px-4 py-2 rounded-t text-shadow ltr:bg-gradient-to-r rtl:bg-gradient-to-l from-black/25 to-black/0"
        style={"background-color: #{get_in(@item, ["Display", "Color"], "#71717a")};"}
      >
        <%= render_slot(@title) %>
      </h3>
      <div class="flex flex-col gap-2">
        <div :if={@inner_block != nil} class="p-4 flex flex-col gap-2">
          <%= render_slot(@inner_block) %>
          <p class="text-right text-sm text-gray-400 dark:text-gray-500">
            <strong>Source</strong>
            <br />
            <%= get_in(@item, ["Source", "Title"]) %>
            <span :if={get_in(@item, ["Source", "Page"])}>
              p.<%= get_in(@item, ["Source", "Page"]) %>
            </span>
          </p>
        </div>
        <div
          :if={Application.get_env(:swornforged, :datasworn_debug, false)}
          class="bg-red-500 rounded-b p-2 text-white"
        >
          <details>
            <summary class="hover:cursor-default">Raw JSON</summary>
            <div class="prose max-w-none align-left">
              <pre><code><%= Jason.encode!(@item, pretty: true, escape: :html_safe) %></code></pre>
            </div>
          </details>
        </div>
      </div>
    </div>
    """
  end
end
