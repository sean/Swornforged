# Swornforged

[![status-badge](https://codeberg-ci.sean.wtf/api/badges/sean/Swornforged/status.svg)](https://codeberg-ci.sean.wtf/sean/Swornforged)

An unofficial online SRD for the RPG Ironsworn: Starforged.

Built with [Dataforged](https://github.com/rsek/dataforged) by [rsek](https://github.com/rsek/).
